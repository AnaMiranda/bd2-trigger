CREATE TABLE historico(
    operacao          varchar(255)   NOT NULL,
    usuario           varchar(255)   NOT NULL,
    data         	  varchar(255)   NOT NULL,
	registro_ante     varchar(255)   NOT NULL,
	registro_novo     varchar(255)   NOT NULL
);

CREATE FUNCTION f_trg_ins_upd_del_modelo()
RETURNS trigger
AS $$
BEGIN
	INSERT INTO historico VALUES(TG_OP, current_user, current_timestamp, 
								 OLD.codigo::varchar || ' ' || OLD.nome || ' ' || OLD.cnpj_fabricante,
								 NEW.codigo::varchar || ' ' || NEW.nome || ' ' || NEW.cnpj_fabricante);
	raise notice 'Tipo da operação %', TG_OP;
	raise notice 'Usuario %', current_user;
	raise notice 'Data %', current_timestamp;
	IF (TG_OP = 'DELETE' or TG_OP = 'UPDATE') THEN 
		raise notice 'Estado do registro antes da operação %', OLD;
	END IF;
	IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
		raise notice 'Estado do registro depois da operação %', NEW;
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_ins_upd_del_modelo
BEFORE INSERT or UPDATE or DELETE
ON modelo
FOR EACH ROW
EXECUTE PROCEDURE f_trg_ins_upd_del_modelo()

CREATE VIEW v_audit_modelo AS SELECT * FROM historico;

UPDATE modelo SET nome = 'Fusca' WHERE nome = 'Fuscas'

SELECT * FROM v_audit_modelo
